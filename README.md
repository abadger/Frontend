# Paypark parking - Frontend using Click2Park v3 API

This is a standalone application written in Vue to demonstrate the *new* API v3 calls.

https://directparking.netfm.org/api/v3/

Use the Django admin to create new parking products, and extras associated with these products

https://directparking.netfm.org/admin/

You can see a copy of the frontend running at:-

https://frontend.netfm.org/

Enter two dates and "xxx" as the promocode to see ful demo

## MacOS setup

Install npm - various web guides for this...

https://gist.github.com/nerdenough/d288f2e732637f55f9858070c6b8b15b

clone the repo
```
git clone https://github.com/isaacjoy/directparking.git
cd directparking
npm install
npm run dev
```
browse http://localhost:8080


> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
