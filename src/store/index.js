import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store(
    {
        state: {
            results: [],
            promoUsed: null,
            startDate: null,
            endDate: null,
            booker: '',
            bookeremail: ''
        },
        mutations: {
            addResults (state, results) {
                state.results.push(results)
            },
            addPromoUsed (state, truth) {
                state.promoUsed = truth
            },
            setStartDate (state, date) {
                state.startDate = date
            },
            setEndDate (state, date) {
                state.endDate = date
            },
            setBooker (state, name) {
                state.booker = name
            },
            setBookerEmail (state, email) {
                state.bookeremail = email
            }
        }
    }
)